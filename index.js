
// Set up dpendencies
const express = require ("express");
const mongoose = require ("mongoose");
const cors = require("cors");


// This allows us to use all the routes defined in "userRoute.js"
const userRoute = require("./routes/userRoute");

const courseRoute = require ("./routes/courseRoute");

// Server
const app = express();



// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));



// Allows all the user routes created in "userRoute.js" file to use "/user" as route (resources)
// localhost: 4000/users
app.use("/users", userRoute);

app.use("/courses", courseRoute);


// Database Connection
mongoose.connect("mongodb+srv://zmasujer:admin123@zuitt-bootcamp.kqj4tzd.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log("Now Connected to cloud database!"));



// Server Listening
// Will use the defined port number for the application whenever environment variable is available ot use port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));
